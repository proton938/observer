import { ChangeDetectionStrategy, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { fromEvent, Observable, of, range, Subject, Subscription, } from 'rxjs';

import { map, mergeMap } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.Default,
  encapsulation: ViewEncapsulation.Emulated,
})
  export class AppComponent implements OnInit {

  testShow = false;
  title = 'observer';
  user: User[];
  private listenerInput = () => document.getElementById('testText');  // элемент наблюдения
  private subscription: Subscription;

  @Input() _subjectStream = new Subject<number>();
  get subjectStream() {
    const subsForPush: Subscription = this._subjectStream.subscribe((value: any) => {
      console.log(value);
      subsForPush.unsubscribe();
      return value;
    });
    return true;
  }


  private arr = [5, 8, 9, 20, 6, 2, 7];
  private stream1 = of( 5, 8, 9, 20, 6, 2, 7 );
  private stream2 = of( 1, 3, 5 );
  private stream3 = of( 6, 12, 28, 49, 35 );

  private streams = {
    stream1: of( 5, 8, 9, 20, 6, 2, 7 ),
    stream2: of( 1, 3, 5 ),
    stream3: of( 6, 12, 28, 49, 35 ),
  };

  subscribers: ISubscribers[] = [];

  constructor(
    private http: HttpClient,
  ) {
  }

  ngOnInit() {
    for (let i = 0; i < 5; i++) {
      this.subscribers.push({});
    }

    this.http.get<User[]>('assets/user.json').subscribe(
      data => {
        this.user = data;
      });

    // Observable.prototype.pipe((n) => n);
    // of( 5, 8, 9, 20, 6, 4, 7 ).pipe(map(x => x + 1), map(x => x - 2)).subscribe((z: number) => console.log('z = ' + z));

    const rng = range(1, 3).subscribe((x: any) => x);

    /*
    let counter = 0;
    this.stream1
      .pipe(
        map((val, i) => {
          counter > 2 ? counter = 1 : counter++;
          console.log('stream' + counter);
          return i;
        }),
        mergeMap(() => this.streams['stream' + counter])
        // mergeMap((): any => range(1, 3))
      )
      .subscribe((val => console.log(val)));
     */

    /*
    range(1, 3)
      .pipe(
        map((value): any => {
          console.log('stream' + value);
          return value;
        }),
        mergeMap((value) => this.streams['stream' + value])
      )
      .subscribe((value) => console.log(value));
     */

    of(this.stream1, this.stream2, this.stream3)
      .pipe(
        map((value, index): any => {
          console.log('stream' + (index + 1));
          return value;
        }),
        mergeMap(value => value)
      )
      .subscribe(value => console.log(value));

    /*
    concat(this.stream1, this.stream2, this.stream3).pipe().subscribe((response => {
      console.log(response);
    }));
     */

    this.subjectInterval();
  }

  /**
   * создание потока наблюдения
   */
  formInputEmitter(): Observable<Event> {
    return fromEvent(this.listenerInput(), 'input');
  }

  startStream() {
    this.subscription = this.formInputEmitter()
      // .pipe(map((field: any) => (field.target.value * 1.2).toFixed(1)))
      .subscribe((event: HTMLElementEventMap['input']) => {
        if (event.target instanceof HTMLInputElement) {
          console.log(event.target.value);
        }
      });
  }

  endStream() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  /**
   * подписаться на поток или отписаться
   * @param index
   */
  subsStreams(index: number) {
    if (!this.subscribers[index].subscription || this.subscribers[index].subscription.closed) {
      this.subscribers[index].subscription = this.formInputEmitter().subscribe(
        (event: HTMLElementEventMap['input']) => {
          const displayStream = document.getElementById('displayStream_' + index);
          if (event.target instanceof HTMLInputElement && displayStream instanceof HTMLInputElement) {
            displayStream.value = event.target.value;
          }
        }
      );
    } else {
      this.subscribers[index].subscription.unsubscribe();
    }
  }


  private subjectInterval() {
    this._subjectStream.next(Math.round(Math.random() * 1000));
    setTimeout(() => this.subjectInterval(), 1000);
  }

  private ofInterval() {
    setTimeout(() => this.ofInterval(), 1000);
    return of(Math.round(Math.random() * 1000));
  }

  subsSubjectStream(index: number) {
    if (!this.subscribers[index].subscription || this.subscribers[index].subscription.closed) {
      this.subscribers[index].subscription = this._subjectStream.subscribe(
        (value: number) => {
          const displayStream = document.getElementById('displayStream_' + index);
          if (displayStream instanceof HTMLInputElement) {
            displayStream.value = value.toFixed();
          }
        }
      );
    } else {
      this.subscribers[index].subscription.unsubscribe();
    }
  }

}

interface User {
  name: string;
  age: number;
}

interface ISubscribers {
  subscription?: Subscription;
}

